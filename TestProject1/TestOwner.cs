﻿using NUnit.Allure.Attributes;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace TestProject1
{
    [TestFixture]
    [Parallelizable]
    public class TestOwner : Hooks
    {
        public TestOwner() : base(BrowserType.Chrome)
        {
        }

        [Test]
        [AllureSuite("Test work with owner")]
        public void AddTestOwner()
        {
            PageElement.Navigation.OwnerTab.Click();
            PageElement.Element.OwnerAdd.Click();
            PageElement.Element.Button.Click();
            PageElement.Element.FirstName.Click();
            PageElement.Element.FirstName.SendKeys(Faker.Name.FirstName());
            PageElement.Element.LastName.Click();
            PageElement.Element.LastName.SendKeys(Faker.Name.LastName());
            PageElement.Element.Address.Click();
            PageElement.Element.Address.SendKeys(Faker.Address.Country());
            PageElement.Element.City.Click();
            PageElement.Element.City.SendKeys(Faker.Address.Province());
            PageElement.Element.Telephone.Click();
            PageElement.Element.Telephone.SendKeys(Faker.Number.RandomNumber(10000,10000).ToString());
            PageElement.Element.AddOwner.Click();
            PageElement.Element.ChoseLastOwner.Click();
            PageElement.Element.EditOwner.Click();
            PageElement.Element.FirstName.Click();
            PageElement.Element.FirstName.Clear();
            PageElement.Element.FirstName.SendKeys(Faker.Name.FirstName());
            PageElement.Element.LastName.Click();
            PageElement.Element.LastName.Clear();
            PageElement.Element.LastName.SendKeys(Faker.Name.FirstName());
            PageElement.Element.Address.Click();
            PageElement.Element.Address.Clear();
            PageElement.Element.Address.SendKeys(Faker.Address.Country());
            PageElement.Element.City.Click();
            PageElement.Element.City.Clear();
            PageElement.Element.City.SendKeys(Faker.Address.Province());
            PageElement.Element.Telephone.Click();
            PageElement.Element.Telephone.Clear();
            PageElement.Element.Telephone.SendKeys(Faker.Number.RandomNumber(10000,10000).ToString());
            PageElement.Element.UpdateOwner.Click();
            PageElement.Element.AddNewPet.Click();
            PageElement.Element.Name.Click();
            PageElement.Element.Name.SendKeys(Faker.Name.FirstName());
            PageElement.Element.BirthDate.Click();
            PageElement.Element.BirthDate.SendKeys($"{Faker.Date.Year()}/{Faker.Date.Month()}/{Faker.Date.Day()}");
            PageElement.Element.SavePet.Click();
        }
    }
}