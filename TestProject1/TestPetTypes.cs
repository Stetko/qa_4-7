﻿using NUnit.Allure.Attributes;
using NUnit.Framework;
using OpenQA.Selenium;

namespace TestProject1
{
    [TestFixture]
    [Parallelizable]
    public class TestPetTypes : Hooks
    {
        public TestPetTypes() : base(BrowserType.Chrome)
        {
        }

        [Test]
        [AllureSuite("Test work with pet types")]
        public void AddTestOwner()
        {   
            PageElement.Navigation.PetTypes.Click();
            PageElement.Element.UpdateFirstPetType.Click();
            PageElement.Element.Name.Click();
            PageElement.Element.Name.Clear();
            PageElement.Element.Name.SendKeys(Faker.Name.FirstName());
            PageElement.Element.UpdatePetType.Click();
            PageElement.Element.AddPet.Click();
            PageElement.Element.Name.Click();
            PageElement.Element.Name.SendKeys(Faker.Name.FullName());
            PageElement.Element.SavePetType.Click();
            PageElement.Element.DeletePetType.Click();
        }
    }
}