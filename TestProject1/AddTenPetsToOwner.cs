﻿using NUnit.Allure.Attributes;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace TestProject1
{
    [TestFixture]
    [Parallelizable]
    public class AddTenPetsToOwner : Hooks
    {
        public AddTenPetsToOwner() : base(BrowserType.Chrome)
        {
        }

        [Test]
        [AllureSuite("Test work with owner")]
        public void AddTestOwner()
        {
            PageElement.Navigation.OwnerTab.Click();
            PageElement.Navigation.ViewOwners.Click();
            PageElement.Element.OpenFirstOwner.Click();
            for (int i = 0; i < 10; i++)
            {
                Helpers.Wait(5);
                PageElement.Element.AddNewPet.Click();
                PageElement.Element.Name.Click();
                PageElement.Element.Name.SendKeys(Faker.Name.FirstName());
                PageElement.Element.BirthDate.Click();
                PageElement.Element.BirthDate.SendKeys($"{Faker.Date.Year()}/{Faker.Date.Month()}/{Faker.Date.Day()}");
                PageElement.Element.Type.Click();
                PageElement.Element.TypeSelect.Click();
                PageElement.Element.SavePet.Click();
            }
        }
    }
}