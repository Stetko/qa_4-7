﻿using OpenQA.Selenium;
using System;
using System.Threading;
using NUnit.Allure.Steps;

namespace TestProject1
{
    public static class Helpers
    {
        public static Boolean IsEditable(IWebElement element)
        {
            return element.Enabled && element.GetAttribute("readonly") == null;
        }

        public static void Wait(int time = 3)
        {
            Thread.Sleep(TimeSpan.FromSeconds(time));
        }

        [AllureStep("Clear input and input new value")]
        public static void ClearAndType(IWebElement element, string text)
        {
            element.Clear();
            element.SendKeys(text);
        }

        [AllureStep("Click on element and wait")]
        public static void ClickAndWait(IWebElement element, int time = 3)
        {
            element.Click();
            Wait(time);
        }
    }
}