using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.DevTools.V94.Debugger;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Safari;

namespace TestProject1
{
    public enum BrowserType
    {
        Chrome,
        Safari,
        Firefox
    }

    public class Hooks : TestBase
    {
        private BrowserType browserType;
        private IConfigurationRoot Configuration { get; set; }

        public Hooks(BrowserType browser)
        {
            browserType = browser;
        }

        [SetUp]
        public void createBrowser()
        {
            ChooseBrowser(browserType);
            driver.Manage().Window.Size = new System.Drawing.Size(1600, 900);
            driver.Navigate().GoToUrl("http://20.50.171.10:8080/");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(500);
            driver.Manage().Timeouts().AsynchronousJavaScript = TimeSpan.FromSeconds(500);
            PageElement = new Pages(driver);
        }

        public void ChooseBrowser(BrowserType browser)
        {
            Dictionary<string, object> additionalSelenoidCapabilities = new Dictionary<string, object>();
            additionalSelenoidCapabilities["name"] = "Lab 7 PI-61";
            additionalSelenoidCapabilities["enableVNC"] = true;
            additionalSelenoidCapabilities["enableVideo"] = true;

            switch (browser)
            {
                case BrowserType.Chrome:
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddAdditionalOption("selenoid:options", additionalSelenoidCapabilities);
                    driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"),
                        chromeOptions.ToCapabilities());
                    break;
                
                case BrowserType.Firefox:
                    var firefoxOptions = new FirefoxOptions();
                    firefoxOptions.AddAdditionalOption("selenoid:options", additionalSelenoidCapabilities);
                    driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"),
                        firefoxOptions.ToCapabilities());
                    break;
             
                default:
                    driver = new ChromeDriver();
                    break;
            }
        }
    }
}