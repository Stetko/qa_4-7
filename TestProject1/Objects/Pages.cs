﻿using OpenQA.Selenium;
using TestProject1.Objects;
using TestProject1.Components;

namespace TestProject1
{
    public class Pages
    {
        protected IWebDriver driver;
        public Pages(IWebDriver driver) => this.driver = driver;
        public elements Element => new elements(driver);
        public NavigationComponent Navigation => new NavigationComponent(driver);
    }
}
