﻿using OpenQA.Selenium;

namespace TestProject1.Objects
{
    public class BasePage
    {
        protected IWebDriver driver;

        public BasePage(IWebDriver driver) => this.driver = driver;
    }
}