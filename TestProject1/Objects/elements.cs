﻿using OpenQA.Selenium;
using TestProject1.Objects;

namespace TestProject1.Objects
{
    public class elements : BasePage
    {
        public elements(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement FirstName => driver.FindElement(By.Id("firstName"));
        public IWebElement LastName => driver.FindElement(By.Id("lastName"));
        public IWebElement Address => driver.FindElement(By.Id("address"));
        public IWebElement City => driver.FindElement(By.Id("city"));
        public IWebElement Telephone => driver.FindElement(By.Id("telephone"));
        public IWebElement Button => driver.FindElement(By.CssSelector(".btn"));
        public IWebElement Name => driver.FindElement(By.Id("name"));
        public IWebElement AddOwner => driver.FindElement(By.CssSelector(".addOwner"));
        public IWebElement ChoseLastOwner => driver.FindElement(By.CssSelector(".petOwner:last-child a"));
        public IWebElement EditOwner => driver.FindElement(By.CssSelector(".editOwner"));
        public IWebElement UpdateOwner => driver.FindElement(By.CssSelector(".updateOwner"));
        public IWebElement AddNewPet => driver.FindElement(By.CssSelector(".addNewPet"));
        public IWebElement BirthDate => driver.FindElement(By.Name("birthDate"));
        public IWebElement SavePet => driver.FindElement(By.CssSelector(".savePet"));
        public IWebElement UpdateFirstPetType => driver.FindElement(By.CssSelector("tr:first-child .editPet"));
        public IWebElement UpdatePetType => driver.FindElement(By.CssSelector(".updatePetType"));
        public IWebElement SavePetType => driver.FindElement(By.CssSelector(".saveType"));
        public IWebElement DeletePetType => driver.FindElement(By.CssSelector("tr:last-child .deletePet"));
        public IWebElement AddPet => driver.FindElement(By.CssSelector(".addPet"));
        public IWebElement OwnerAdd => driver.FindElement(By.CssSelector(".open li:nth-child(1) > a"));
        public IWebElement OpenFirstOwner => driver.FindElement(By.CssSelector(".ownerFullName > a"));
        public IWebElement Type => driver.FindElement(By.Id("type"));
        public IWebElement TypeSelect => driver.FindElement(By.TagName("option"));
    }
}