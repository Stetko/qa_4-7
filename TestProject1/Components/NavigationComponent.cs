﻿using OpenQA.Selenium;
using TestProject1.Objects;

namespace TestProject1.Components
{
    public class NavigationComponent : BasePage
    {
        public NavigationComponent(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement OwnerTab => driver.FindElement(By.CssSelector(".ownerTab"));

        public IWebElement PetTypes => driver.FindElement(By.CssSelector("li:nth-child(4) > a"));

        public IWebElement VetsTab => driver.FindElement(By.CssSelector(".vetsTab"));

        public IWebElement NewVet => driver.FindElement(By.CssSelector(".open li:nth-child(2) span:nth-child(2)"));
        public IWebElement NewOwner => driver.FindElement(By.CssSelector(".open li:nth-child(2) > a"));
        public IWebElement ViewOwners => driver.FindElement(By.CssSelector(".open li:first-child > a"));

        public IWebElement Specialties => driver.FindElement(By.CssSelector("li:nth-child(5) > a"));
    
        }
}