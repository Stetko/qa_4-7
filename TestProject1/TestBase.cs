﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using NUnit.Framework.Interfaces;
using NUnit.Allure.Core;
using Allure.Commons;


namespace TestProject1
{
    [AllureNUnit]
    public class TestBase
    {
        protected IWebDriver driver;
        public Pages PageElement { get; set; }
        private IConfigurationRoot Configuration { get; set; }
        public IDictionary<string, object> vars { get; private set; }
        protected IJavaScriptExecutor js;



        [TearDown]
        protected void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                var screenshot = ((ITakesScreenshot) driver).GetScreenshot();
                var filename = TestContext.CurrentContext.Test.MethodName + "_screenshot" + DateTime.Now.Ticks + ".png";
                var path =
                    $"C:\\Users\\Stetko\\RiderProjects\\TestProject1\\TestProject1\\bin\\Debug\\allure-results\\{filename}";
                screenshot.SaveAsFile(path, ScreenshotImageFormat.Png);
                AllureLifecycle.Instance.AddAttachment(filename, "image/png", path);
            }
        
            driver.Quit();
        }
    }
}